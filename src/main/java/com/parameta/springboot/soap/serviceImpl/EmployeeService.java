package com.parameta.springboot.soap.serviceImpl;

import com.parameta.springboot.soap.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parameta.springboot.soap.repo.EmployeeRepository;
import com.parameta.springboot.soap.service.IEmployeeService;

@Service
public class EmployeeService implements IEmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;


    @Override
    public void AddEmployee(Employee employee) {
        Employee data = getEmployeeByDocumentNumber(employee.getDocumentNumber());
        if (data == null)
            employeeRepository.save(employee);
    }

    private Employee getEmployeeByDocumentNumber(String documentNumber) {

        Employee obj = employeeRepository.findByDocumentNumber(documentNumber);
        return obj;

    }


}
