package com.parameta.springboot.soap.repo;

import com.parameta.springboot.soap.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String>{


	Employee findByDocumentNumber(String documentNumber);

}
