package com.parameta.springboot.soap.endpoints;

import com.parameta.springboot.soap.entities.Employee;
import com.parameta.springboot.soap.serviceImpl.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.parameta.springboot.soap.interfaces.AddEmployeeRequest;
import com.parameta.springboot.soap.interfaces.AddEmployeeResponse;
import com.parameta.springboot.soap.interfaces.ServiceStatus;

@Endpoint
public class EmployeeEndpoint {

	private static final String NAMESPACE_URI = "http://interfaces.soap.springboot.parameta.com";

	@Autowired
	private EmployeeService employeeService;


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addEmployeeRequest")
	@ResponsePayload
	public AddEmployeeResponse addEmployee(@RequestPayload AddEmployeeRequest request) {
		AddEmployeeResponse response = new AddEmployeeResponse();
		ServiceStatus serviceStatus = new ServiceStatus();

		Employee employee = new Employee();
		BeanUtils.copyProperties(request.getEmployeeInfo(), employee);
		employee.setDateOfBirth(request.getEmployeeInfo().getDateOfBirth().toGregorianCalendar().getTime());
		employee.setLinkingDate(request.getEmployeeInfo().getLinkingDate().toGregorianCalendar().getTime());
		employeeService.AddEmployee(employee);
		serviceStatus.setStatus("SUCCESS");
 	    serviceStatus.setMessage("Content Added Successfully");
		response.setServiceStatus(serviceStatus);
		return response;
	}

}
