**ESTE SERVICIO SOAP SE ENCARGA DE GUARDAR EN BASE DE DATOS EL EMPLEADO Y ES CONSUMIDO POR EL SERVICIO REST**

Ejemplo de como consumirlo por soapUI

URI: http://localhost:2026/parametaService

BODY:

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:int="http://interfaces.soap.springboot.parameta.com">
   <soapenv:Header/>
   <soapenv:Body>
      <int:addEmployeeRequest>
         <int:employeeInfo>
		  <int:employeeId>?</int:employeeId>
            <int:name>Nestor</int:name>
            <int:lastname>Diaz</int:lastname>
            <int:documentType>CC</int:documentType>
            <int:documentNumber>1345</int:documentNumber>
            <int:dateOfBirth>1997-08-21</int:dateOfBirth>
            <int:linkingDate>2020-07-26</int:linkingDate>
            <int:position>software</int:position>
            <int:salary>300000</int:salary>
         </int:employeeInfo>
      </int:addEmployeeRequest>
   </soapenv:Body>
</soapenv:Envelope>